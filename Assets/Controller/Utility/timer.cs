﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class timer : MonoBehaviour
{

    public Text textTimer;
    private float startTime;
    public Image bar;
    

    // Use this for initialization
    void Start()
    {//590 for 10 min
        bar = this.GetComponent<Image>();
        startTime = 590;
        
    }

    // Update is called once per frame
    void Update()
    {
        float t = startTime - Time.time;
        if (t > 0)
        {
            string minutes = ((int)t / 59).ToString();
            string seconds = (t % 59).ToString("f0");
			if (textTimer) {
				textTimer.text = minutes + ":" + seconds;
			}
            //t -= Time.deltaTime;
			if (bar) {
				bar.fillAmount = t / startTime;
			}
        }

    }



}


