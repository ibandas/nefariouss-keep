﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Enemy_Chase : MonoBehaviour {
	private NavMeshAgent myAgent;
	public Transform target;
	public Camera targetCamera;

	// Use this for initialization
	void Start () {
		myAgent = GetComponent<NavMeshAgent> ();
	}
	
	// Update is called once per frame
	void Update () {
		myAgent.SetDestination (target.position);
		if (target != null) {
			transform.LookAt(transform.position + targetCamera.transform.rotation * Vector3.left,
			targetCamera.transform.rotation * Vector3.up);
		}
	}
}
