﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy_Spawn : MonoBehaviour {
	public GameObject Crawler;
	public AudioSource audio;
	private float startTime;
	private bool hasPlayed;
	// Use this for initialization
	void Start () {
		startTime = 590;
		hasPlayed = false;
	}
	
	// Update is called once per frame
	void Update () 
	{	
		float t = startTime - Time.time;
		if (t < 100 && !hasPlayed) {
			Crawler.SetActive (true);
			audio.Play ();
			hasPlayed = true;
		}
	}
}
