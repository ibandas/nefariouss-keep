﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Flashlight : MonoBehaviour 
{
	public Light flashLight; //Holds the flashlight light object
	public AudioSource audioSource; //The object from which the audio comes from

	public AudioClip soundFlashlightOn; //sound for when the flashlight is turned on
	public AudioClip soundFlashlightOff; //sound for when the flashlight is turned off

	private bool isActive; //Checks to see if the flashlight is on (true) or off (false)

	void Start () //The game starts with the flashlight being on
	{
		isActive = true;
	}

	void Update () //Checks each frame during the game
	{
		if (Input.GetKeyDown (KeyCode.X)) { //If X key is pressed
			if (isActive == false) { // If the flashlight is off
				flashLight.enabled = true; //Flashlight turns on
				isActive = true;
				audioSource.PlayOneShot (soundFlashlightOn);
			} else if (isActive == true) { //If flashlight is on
				flashLight.enabled = false; //Flashlight turns off
				isActive = false;
				audioSource.PlayOneShot (soundFlashlightOff);
			}
		}
	}
}
