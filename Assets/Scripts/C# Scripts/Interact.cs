﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Interact : MonoBehaviour
{
	public string interactButton;

	public float interactDistance = 4f;
	public LayerMask interactLayer; //Filter

	public Image interactIcon1; // Picture to show if you can interact or not
	public Image interactIcon2;

	public bool isInteracting;

	// Use this for initialization
	void Start ()
	{
		//Set Interact icon to be invisible
		if(interactIcon1 != null)
		{
			interactIcon1.enabled = false;
		}
		if (interactIcon2 != null) {
			interactIcon2.enabled = false;
		}
	}

	// Update is called once per frame
	void Update ()
	{
		Ray ray = new Ray(transform.position, transform.forward);
		RaycastHit hit;

		//Shoots a ray
		if(Physics.Raycast(ray, out hit, interactDistance, interactLayer))
		{
			//Checks if we are not interacting
			if (isInteracting == false && !hit.collider.CompareTag ("Throwable")) {
				if (interactIcon1 != null) {
					interactIcon1.enabled = true;
				}

				//If we press the interaction button
				if (Input.GetButtonDown (interactButton)) {
					if (hit.collider.CompareTag ("Safe")) {
						hit.collider.GetComponent<Safe> ().ShowSafeCanvas ();
					}
				}
			} else if (isInteracting == false && hit.collider.CompareTag ("Throwable")) {
				if (interactIcon2 != null) {
					interactIcon2.enabled = true;
				}
			}
		}
		else
		{
			interactIcon1.enabled = false;
			interactIcon2.enabled = false;
		}
	}
}
