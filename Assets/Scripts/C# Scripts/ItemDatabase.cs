﻿/*using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Collections.Generic;
using System.IO;


public class ItemDatabase : MonoBehaviour
{
    private List<Item> database = new List<Item>(); //stores all items within database
    private JsonData itemData; //holds Json data that is pulled in 

    void Start()
    {
        /* Item item = new Item(0, "Knife", 5);
         database.Add(item);
         Debug.Log(database[0].Title); //
        itemData = JsonMapper.ToObject(File.ReadAllText(Application.dataPath + "/StreamingAssets/Items.json")); //allows us to take c# item and convert it to Json object and viseversa
        
        //below to test if ITEM was found:
        //ConstructItemDatabase();
        //Debug.Log(database[1].Title);
    }

    public Item FetchItemByID(int id) //returns item by ID
    {
        for (int i = 0; i < database.Count; i++)
        {
            if (database[i].ID == id)
            {
                return database[i];
            }
        }
        return null;
    }

    void ConstructItemDatabase() //contain each value key pair from itemData
    {
        for (int i = 0; i < itemData.Count; i++) //looping through each element in itemData
        {
            database.Add(new Item((int)itemData[i]["id"], itemData[i]["title"].ToString(), (int)itemData[i]["value"], itemData[i]["slug"].ToString()));
            //example for a bool: (bool)itemData[i]["stackable"]
        }
    }
}

public class Item //properties for item
{
    public int ID { get; set; }
    public string Title { get; set; }
    public int Value { get; set; }
    public string Slug { get; set; }
    public Sprite Sprite { get; set; }

    public Item(int id, string title, int value, string slug)
    {
        //constructors
        this.ID = ID;
        this.Title = title;
        this.Value = value;
        this.Slug = slug;
        this.Sprite = Resources.Load<Sprite>("Sprites/Items/" + slug);
    }

    public Item()
    {
        //empty item to use when delete item from inventory slot
        this.ID = -1;
    }
}
*/