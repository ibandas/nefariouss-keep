﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PopUpScare : MonoBehaviour {
	private bool hasPlayed;
	public GameObject scareCrow;
	public GameObject audioTrigger;

	void OnTriggerEnter(Collider triggerEnterCollider)
	{
		if ((triggerEnterCollider.CompareTag ("Player") || triggerEnterCollider.CompareTag("Rosary")) && !hasPlayed) {
			scareCrow.SetActive (true);
			audioTrigger.SetActive (true);
		}
	}
}
