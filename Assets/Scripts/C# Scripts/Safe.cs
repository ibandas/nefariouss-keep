﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityStandardAssets.Characters.FirstPerson;

public class Safe : MonoBehaviour {

	public Canvas safeCanvas;
	public GameObject playerObject;

	private int number01 = 1;
	private int number02 = 1;
	private int number03 = 1;
	private int number04 = 1;
	private int number05 = 1;

	public Text textNumber01;
	public Text textNumber02;
	public Text textNumber03;
	public Text textNumber04;
	public Text textNumber05;

	public bool opened;
	public float doorOpenAngle = 90f;
	public float doorClosedAngle = 0f;

	public float smooth = 2f;

	void Start ()
	{
		safeCanvas.enabled = false;
		opened = false;
	}

	public void ShowSafeCanvas()
	{
		safeCanvas.enabled = true;
		//Disable the Player - Controller
		playerObject.GetComponent<FirstPersonController>().enabled = false;
		//Unlocks the (Mouse)Cursor and makes it visible
		Cursor.lockState = CursorLockMode.None;
		Cursor.visible = true;
	}

	void Update () 
	{
		if (Input.GetButtonDown ("Cancel")) {
			Cursor.lockState = CursorLockMode.Locked;
			Cursor.visible = false;
			playerObject.GetComponent<FirstPersonController> ().enabled = true;
			safeCanvas.enabled = false;
		}

		if (number01 == 7 && number02 == 3 && number03 == 5 && number04 == 2 && number05 == 8) {
			opened = true;
		}

		if (opened == true) {
			Cursor.lockState = CursorLockMode.Locked;
			Cursor.visible = false;
			playerObject.GetComponent<FirstPersonController> ().enabled = true;
			safeCanvas.enabled = false;
			gameObject.layer = 0;
			UnlockSafe ();
		}
	}
	public void IncreaseNumber(int _number)
	{
		if (_number == 1) {
			number01++;
			textNumber01.text = number01.ToString ();

			if (number01 > 9) {
				number01 = 1;
				textNumber01.text = number01.ToString ();
			}
		} else if (_number == 2) {
			number02++;
			textNumber02.text = number02.ToString ();

			if (number02 > 9) {
				number02 = 1;
				textNumber02.text = number02.ToString ();
			}
		} else if (_number == 3) {
			number03++;
			textNumber03.text = number03.ToString ();

			if (number03 > 9) {
				number03 = 1;
				textNumber03.text = number03.ToString ();
			}
		} else if (_number == 4) {
			number04++;
			textNumber04.text = number04.ToString ();

			if (number04 > 9) {
				number04 = 1;
				textNumber04.text = number04.ToString ();
			} 
		} else if (_number == 5) {
			number05++;
			textNumber05.text = number05.ToString ();

			if (number05 > 9) {
				number05 = 1;
				textNumber05.text = number05.ToString ();
			}
		}
	}
	public void DecreaseNumber(int _number)
	{
		if (_number == 1) {
			number01--;
			textNumber01.text = number01.ToString ();

			if (number01 < 1) {
				number01 = 9;
				textNumber01.text = number01.ToString ();
			}
		} else if (_number == 2) {
			number02--;
			textNumber02.text = number02.ToString ();

			if (number02 < 1) {
				number02 = 9;
				textNumber02.text = number02.ToString ();
			}
		} else if (_number == 3) {
			number03--;
			textNumber03.text = number03.ToString ();

			if (number03 < 1) {
				number03 = 9;
				textNumber03.text = number03.ToString ();
			}
		} else if (_number == 4) {
			number04--;
			textNumber04.text = number04.ToString ();

			if (number04 < 1) {
				number04 = 9;
				textNumber04.text = number04.ToString ();
			} 
		} else if (_number == 5) {
			number05--;
			textNumber05.text = number05.ToString ();

			if (number05 < 1) {
				number05 = 9;
				textNumber05.text = number05.ToString ();
			}
		}
	}
	//Open the safe
	void UnlockSafe()
	{
		Quaternion targetRotationOpen = Quaternion.Euler (doorOpenAngle, 0, -90);
		transform.localRotation = Quaternion.Slerp (transform.localRotation, targetRotationOpen, smooth * Time.deltaTime);
	}
}
